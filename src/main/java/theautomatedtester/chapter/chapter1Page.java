package theautomatedtester.chapter;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;

public class chapter1Page {
    public static Condition textAssertthatthistextisonthepage = text("Assert that this text is on the page");

    public static SelenideElement textElementAssert() {
        return $("#divontheleft");
    }
    public static SelenideElement linkHomePage() {
        return $(By.xpath("//a[contains(text(),'Home Page')]"));
    }
}
