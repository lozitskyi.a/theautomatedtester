package theautomatedtester;

import com.codeborne.selenide.Configuration;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.BeforeClass;

/**
 * Base script functionality, can be used for all Selenium scripts.
 */
public class BaseScript {

    static final String DEFAULT_BASE_URL = "http://book.theautomatedtester.co.uk/";

    //configuration
    @BeforeClass
    public void configuration() throws Exception {
        Configuration.browser = "chrome";
        Configuration.browserSize = "1600x900";
        Configuration.timeout = 10000;
        Configuration.headless = true;
        System.setProperty("chromeoptions.args", "--disable-gpu");
        System.setProperty("chromeoptions.args", "--no-sandbox");
        System.setProperty("chromeoptions.args", "--disable-dev-shm-usage");
        DesiredCapabilities capabilities = new DesiredCapabilities();
        capabilities.setAcceptInsecureCerts(true);
        Configuration.browserCapabilities = capabilities;
    }
}

