package theautomatedtester;

import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.open;
import static theautomatedtester.chapter.chapter1Page.*;
import static theautomatedtester.home.homePage.linkCharper1;

public class Chapter1Test extends BaseScript {

    @Test
    public void openChapter1Page() {
        open(DEFAULT_BASE_URL);
        linkCharper1().click();
        textElementAssert().shouldHave(textAssertthatthistextisonthepage);
        linkHomePage().click();
        linkCharper1().shouldBe(visible);
    }

}