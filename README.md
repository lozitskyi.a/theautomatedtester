# theautomatedtester
* Steps to run a project:
  * clone a project
  * run a command 'mvn clean test' in the root folder of the project

* Or
  * go to CI/CD -> Jobs
  * click on the Retry button on the right.
 
* Or
  * click on Pipelines item of menu
  * click on Run Pipeline button
  * click on Run Pipeline button again 
  